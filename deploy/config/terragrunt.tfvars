terragrunt = {
  remote_state {
    backend = "s3"
    config {
      bucket         = "activity-scheduler-web-terraform-state"
      key            = "${path_relative_to_include()}/terraform.tfstate"
      region         = "eu-west-1"
      encrypt        = true
      dynamodb_table = "activity-scheduler-web-terraform-state-lock"
    }
  }
}